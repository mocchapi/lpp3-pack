#!/usr/bin/python3

# lpp3-pack: a utility for bundling Luna Player Plus 3ds software into CIA and 3DS files

"""
			Copyright (C) 2023 lisanne

			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as
			published by the Free Software Foundation, either version 3 of the
			License, or (at your option) any later version.

			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
			GNU Affero General Public License for more details.

			You should have received a copy of the GNU Affero General Public License
			along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#
# This is a tool to bundle all files in a lua player plus 3ds project
# into a .CIA or .3ds
#
# More info on LPP-3DS: https://github.com/Rinnegatamante/lpp-3ds
#
# It depends on these programs being installed:
#    3dstool ( https://github.com/dnasdw/3dstool )
#    makerom ( https://github.com/3DSGuy/Project_CTR )
#
# The following are recommended (but not integrated in this tool):
#    bannertool ( https://github.com/Steveice10/bannertool )
#
# The "bundled" directory contains all required files needed to make the builds
# but each can be substituted with arguments, in case a different LPP-3DS version is prefered
# These files originated from:
#    workarounds/cia_workaround.rsf: https://github.com/Rinnegatamante/lpp-3ds/blob/master/cia_workaround.rsf
#    workarounds/gw_workaround.rsf:  https://github.com/Rinnegatamante/lpp-3ds/blob/master/gw_workaround.rsf
#    elfs/normal.elf:                https://github.com/Rinnegatamante/lpp-3ds/releases/tag/r5
#    elfs/unsafe.elf:                https://github.com/Rinnegatamante/lpp-3ds/releases/tag/r5
#    banner.bin:                     https://github.com/Rinnegatamante/lpp-3ds/blob/master/banner.bin
#    icon.bin:                       https://github.com/Rinnegatamante/lpp-3ds/blob/master/icon.bin
#
# To make a simple builds:
#    `python3 lpp3-pack .`
# Assuming a index.lua file exists in the current directory
# It will then put two files named "lpp3_pack_build.3ds" and "lpp3_pack_build.cia" 
# in the current directory
#
# for more info, run
#    `python3 lpp3-pack --help`
#


import os
import sys
import argparse
import tempfile
import subprocess
from pathlib import Path

__SOURCE__  = "https://gitlab.com/mocchapi/lpp3-pack"
__VERSION__ = '1.0.0'

root = Path(os.path.realpath(__file__)).parent
temp = Path(tempfile.gettempdir())

default_3ds_rsf = root/'bundled'/'workarounds'/'gw_workaround.rsf'
default_cia_rsf = root/'bundled'/'workarounds'/'cia_workaround.rsf'

parser = argparse.ArgumentParser(
	prog="lpp3-pack",
	description="a utility for bundling Luna Player Plus 3ds software into CIA and 3DS files",
	epilog="this software is licensed under the AGPLv3.0, for more info see the LICENSE file",
	formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)

parser.add_argument('--tool-3dstool', type=Path, default="3dstool", help="Path to 3dstool executable (Defaults to global name)")
parser.add_argument('--tool-makerom', type=Path, default="3dstool", help="Path to makerom executable (Defaults to global name)")

parser.add_argument('--release', '-r', action='store_true', help=f"Uses the `unsafe` release ELF instead of the default debug version. Ignored if --lpp-elf is given. Shorthand for `--lpp-elf {(root/'bundled'/'elfs'/'unsafe.elf')}`")

parser.add_argument('--lpp-elf', '-elf', type=Path, default=None,            help="Path to a lpp-3ds.elf file, for if you want to use a custom Luna Plus Player 3DS build.")
parser.add_argument('--3ds-rsf','-gwd',  type=Path, default=default_3ds_rsf, help="Path to a gw_workaround.rsf file")
parser.add_argument('--cia-rsf','-cwd',  type=Path, default=default_cia_rsf, help="Path to a cia_workaround.rsf file")

parser.add_argument('--banner','-b', type=Path, help="Path to a banner .bin file. Can be generated with bannertool", default=root/'bundled'/'banner.bin')
parser.add_argument('--icon', '-i',  type=Path, help="Path to either a icon .bin file. can be generated with bannertool", default=root/'bundled'/'icon.bin')

parser.add_argument('--3ds-output','-3o', help="Output path & name for the .3ds build", default=Path("lpp3_pack_build.3ds"), type=Path)
parser.add_argument('--cia-output','-co', help="Output path & name for the .cia build", default=Path("lpp3_pack_build.cia"), type=Path)
parser.add_argument('--romfs',            help="Where to put the generated romfs .bin file", type=Path, default=temp/'romfs.bin')


parser.add_argument('input_directory',  help='Directory to load into romfs. Should contain your index.lua and assets', type=Path)
parser.add_argument('unique_id',        default=None, type=str, help="Unqiue/Title ID in 0x123456 format. If you know what you are doing, you can use --skip-rsf-patch to skip this", nargs='?')

parser.add_argument('--skip-cia',       help="Dont build .CIA", action='store_true')
parser.add_argument('--skip-3ds',       help="Dont build .3DS", action='store_true')
parser.add_argument('--skip-romfs',     help="Dont build romfs (must point --romfs to an existing file)", action='store_true')
parser.add_argument('--skip-rsf-patch', help="Dont patch 3ds-rsf and cia-3sf with a given unique ID")

def path_escape(path_in:Path)->str:
	return str(path_in).replace(' ','\\ ')

def patch_rsf(rsf_path, new_id)->Path:
	with open(rsf_path, 'r') as f:
		text = f.read()

	patched = text.replace('UniqueId                : 0x1337', f'UniqueId                : {new_id}')

	out_path = temp / ('patched_' + rsf_path.name)
	with open(out_path, 'w') as f:
		f.write(patched)
	return out_path

def main():
	args = parser.parse_args(sys.argv[1:])
	argsD = vars(args)

	if not (args.skip_3ds and args.skip_cia):
		if args.unique_id is None and not args.skip_rsf_patch:
			print("ERROR: a unique-id must be given when --skip-rsf-patch is false!")
			print("Supply a random number in the style of 0x123456790 (must start with 0x)")
			print("This number must be the same between updates, so save it somewhere")
			return -1
		elif args.skip_rsf_patch:
			print("WARNING: --skip-rsf-patch is true! Unless you modified your RSFs, this will likely collide with other lpp-3ds programs!")
		if '/' in args.unique_id:
			print("ERROR: your unique ID contains invalid characters. Unique IDs should start with '0x' and contain only numbers")
			print('ID `'+args.unique_id+'`is invalid.')
			return -1
		elif not args.unique_id.startswith('0x'):
			print("ERROR: Unique IDs should start with 0x!")
			print("Your ID should be: 0x" + args.unique_id)
			return -1


	if args.lpp_elf == None:
		if args.release:
			print("Using `unsafe.elf` release build")
			args.lpp_elf = root/'bundled'/'elfs'/'unsafe.elf'
		else:
			print("Using `normal.elf` debug build")
			args.lpp_elf = root/'bundled'/'elfs'/'normal.elf'
	else:
		print("Using custom ELF",args.lpp_elf)

	if not args.skip_romfs:
		print("Making romfs")
		romfs_args = ['3dstool', '--verbose', '--create', '--type', 'romfs', '--file', str(args.romfs), '--romfs-dir', str(args.input_directory)]
		process_out = subprocess.run(
			romfs_args,
			)
		if process_out.returncode != 0:
			print("ERROR!",process_out.returncode)
			return (process_out.returncode)
		print("OK")
		print()
	else:
		if not os.path.exists(str(args.romfs)):
			print("ERROR: if using --skip-romfs, you must point --romfs to a valid romfs.bin file!")
			return -1
		print("Skipped making romfs")
		print()



	if not args.skip_3ds:
		rsf_path = argsD['3ds_rsf']
		if not args.skip_rsf_patch:
			print("Patching 3ds rsf")
			rsf_path = patch_rsf(rsf_path, args.unique_id)
			print("OK")

		print("Making .3ds")
		print("(this can take a while)")
		make_3ds_args = ['makerom', '-v', '-f', 'cci', '-o', str(argsD['3ds_output']), '-rsf', str(rsf_path), '-target', 'd', '-exefslogo', '-elf', str(args.lpp_elf), '-icon', str(args.icon), '-banner', str(args.banner), '-romfs', str(args.romfs)]
		process_out = subprocess.run(
				make_3ds_args,
				)
		if process_out.returncode != 0:
			print("ERROR!",process_out.returncode)
			return (process_out.returncode)
		print("OK")
		print()
	else:
		print("Skipped making .3ds")
		print()

	if not args.skip_cia:
		rsf_path = args.cia_rsf
		if not args.skip_rsf_patch:
			print("Patching cia rsf")
			rsf_path = patch_rsf(rsf_path, args.unique_id)
			print("OK")

		print("Making .cia")
		print("(this can take a while)")

		make_cia_args = ['makerom', '-v', '-f', 'cia', '-o', str(args.cia_output), '-rsf', str(rsf_path), '-target', 't', '-exefslogo', '-elf', str(args.lpp_elf), '-icon', str(args.icon), '-banner', str(args.banner), '-romfs', str(args.romfs)]
		process_out = subprocess.run(
				make_cia_args,
				)
		if process_out.returncode != 0:
			print("ERROR!",process_out.returncode)
			return (process_out.returncode)
		print("OK")
		print()
	else:
		print("Skipped making .cia")
		print()

	print("All done!")


if __name__ == '__main__':
	sys.exit(main())