# lpp3-pack
A utility for bundling Luna Player Plus 3ds software into .CIA and .3DS files  
Uses [LPP-3DS](https://github.com/Rinnegatamante/lpp-3ds)

_____

## Requirements
- 3dstool (https://github.com/dnasdw/3dstool)
- makerom (https://github.com/3DSGuy/Project_CTR)  

[bannertool](https://github.com/Steveice10/bannertool), while not required or integrated in this utility, is still recommended for generating custom icons and banners

## Installation
Python 3.10 is required.  
```bash
pip install git+https://gitlab.com/mocchapi/lpp3-pack.git
```
alternatively, if you have already cloned this repo locally:
```bash
pip install .
```
or:
```bash
python setup.py install
```
This will install the `lpp3-pack` terminal application into your path.  



## Usage

**NOTE:** you must supply a unique-id when making a CIA or 3DS. These should be the same between builds of the same project, but MUST differ from any other program installed on your 3DS. If you already define a UniqueID in a custom rsf file, you can skip this by adding `--skip-rsf-patch`.  
**NOTE:** when using this tool, keep in mind that any resources you load (images, sound, etc) must have their path start with `romfs:/`  
IE "/player_sprite.png" must become "romfs:/player_sprite.png", otherwise it will not find the file and raise an error!

To pack everything in the current directory into a .3DS and .CIA using the `normal` release of lpp-3ds:  
```bash  
python3 lpp3-pack.py . 0x123456
```

To pack everything in the directory `source` into only a .CIA using the `unsafe` release of lpp-3ds:  
```bash  
python3 lpp3-pack ./source 0x789012 --skip-3ds --lpp-elf ./bundled/elfs/unsafe.elf
```

To pack everything in the directory `source` into a .CIA and .3DS and also retain the ROMFS.bin and also use a different version of 3dstool and makerom and also set a custom banner and icon (worlds most convoluted setup):  
```bash  
python3 lpp3-pack ./source 0x012210 --romfs 'romfs.bin' --tool-3dstool my_modified_3dstool --tool-makerom my_modified_makerom --banner my_custom_banner.bin --icon my_custom_icon.bin
```  

For the full info, run  
```bash  
python3 lpp3-pack --help
```