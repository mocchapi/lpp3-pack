import os
from setuptools import setup

import lpp3pack

includes = []
for i in os.walk('bundled'):
	if len(i[2]) > 0:
		beans = []
		for b in i[2]:
			beans.append(i[0] + '/' + b)
		includes.append((i[0], beans))

license = ""
with open('./LICENSE') as f:
	license = f.read()

readme = ""
with open('./README.md') as f:
	readme = f.read()

notice = """
  /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\
     lpp3-pack requires the tools `makerom` and `3dstool` to function!
        Look at https://gitlab.com/mocchapi/lpp3-pack for more info!
  /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\
"""

print(notice)

setup(
	name = 'lpp3-pack',
	description = "A utility for bundling Luna Player Plus 3ds software into .CIA and .3DS files",
	py_modules = ['lpp3pack'],
	long_description = readme,
	long_description_content_type = 'text/markdown',
	version = str(lpp3pack.__VERSION__),
	url = str(lpp3pack.__SOURCE__),
	license = license,

	entry_points = {
		'console_scripts': [
			'lpp3-pack = lpp3pack:main',
			]
	},
	data_files = includes,

	python_requires = '>=3.10',
	classifiers = [
		"Programming Language :: Python :: 3",
		"Operating System :: OS Independent",
		"Environment :: Console",
		"License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
		"Topic :: Software Development",
	]
)

print(notice)